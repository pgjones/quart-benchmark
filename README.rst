Benchmark
=========

This allows a simple benchmark comparison of a few Python web
frameworks. It is mainly useful to compare Quart against Flask.

Usage
-----

This expects that `wrk <https://github.com/wg/wrk>`_ is installed and
available on the path. If so,::

    pip install -r requirements.txt
    python benchmark.py
